﻿using QRCoder;
using System;
using System.Drawing;
using System.Drawing.Imaging;

Console.WriteLine("=======================");
Console.WriteLine("Create QR-code for WiFi");
Console.WriteLine("=======================");
Console.WriteLine();
Console.Write("Enter WiFi name: ");
var name = Console.ReadLine();
Console.Write("Enter WiFi password: ");
var password = Console.ReadLine();
Console.Write("Enter WiFi authentication mode [0 = none, 1 = WEP, 2 = WPA, 3 = WPA2]: ");
var mode = Console.ReadLine();
switch (mode)
{
    case "0":
        mode = "";
        break;

    case "1":
        mode = "WEP";
        break;

    case "2":
        mode = "WPA";
        break;

    case "3":
        mode = "WPA2";
        break;

    default:
        Console.WriteLine($"Unknown mode 'mode'.");
        return;
}

Console.WriteLine();
Console.WriteLine("Creating...");
Bitmap qrCodeImage;
if (mode == "WPA2")
{
    var qrCodeImageAndroid = createQrCode(name, password, "WPA");
    var qrCodeImageIos = createQrCode(name, password, "WPA2");
    var headerHeight = 270;
    var qrCodeHeight = Math.Max(qrCodeImageAndroid.Height, qrCodeImageIos.Height);
    qrCodeImage = new Bitmap((int)Math.Round((qrCodeImageAndroid.Width + qrCodeImageIos.Width) * 1.3), headerHeight + qrCodeHeight);
    var graphics = Graphics.FromImage(qrCodeImage);
    graphics.FillRectangle(new SolidBrush(Color.White), 0, 0, qrCodeImage.Width, qrCodeImage.Height);

    drawText(graphics, $"WiFi: {name}", new PointF(qrCodeImage.Width / 2.0f, 90), 50);
    drawText(graphics, $"Password: {password}", new PointF(qrCodeImage.Width / 2.0f, 165), 30);

    var spacing = (int)Math.Round((qrCodeImage.Width - qrCodeImageAndroid.Width - qrCodeImageIos.Width) / 3.0);
    var rect = new RectangleF(spacing, headerHeight + (int)Math.Round((qrCodeHeight - qrCodeImageAndroid.Height) / 2.0), qrCodeImageAndroid.Width, qrCodeImageAndroid.Height);
    graphics.DrawImage(qrCodeImageAndroid, rect);
    drawText(graphics, "Android", new PointF(rect.X + rect.Width / 2.0f, headerHeight), 40);

    rect = new RectangleF(spacing * 2 + qrCodeImageAndroid.Width, headerHeight + (int)Math.Round((qrCodeHeight - qrCodeImageIos.Height) / 2.0), qrCodeImageIos.Width, qrCodeImageIos.Height);
    graphics.DrawImage(qrCodeImageIos, rect);
    drawText(graphics, "iOS", new PointF(rect.X + rect.Width / 2.0f, headerHeight), 40);
}
else
{
    var qrCodeImageBoth = createQrCode(name, password, mode);
    var headerHeight = 180;
    var qrCodeHeight = qrCodeImageBoth.Height;
    qrCodeImage = new Bitmap((int)Math.Round(qrCodeImageBoth.Width * 1.3), headerHeight + qrCodeHeight);
    var graphics = Graphics.FromImage(qrCodeImage);
    graphics.FillRectangle(new SolidBrush(Color.White), 0, 0, qrCodeImage.Width, qrCodeImage.Height);

    drawText(graphics, $"WiFi: {name}", new PointF(qrCodeImage.Width / 2.0f, 90), 50);
    drawText(graphics, $"Password: {password}", new PointF(qrCodeImage.Width / 2.0f, 165), 30);

    var rect = new RectangleF((int)Math.Round((qrCodeImage.Width - qrCodeImageBoth.Width) / 2.0), headerHeight + (int)Math.Round((qrCodeHeight - qrCodeImageBoth.Height) / 2.0), qrCodeImageBoth.Width, qrCodeImageBoth.Height);
    graphics.DrawImage(qrCodeImageBoth, rect);
}
var filename = $"{name}.png";
qrCodeImage.Save(filename, ImageFormat.Png);
Console.WriteLine($"Saved as: {filename}");

Bitmap createQrCode(string name, string password, string mode)
{
    using var qrGenerator = new QRCodeGenerator();
    var payload = $"WIFI:T:{mode};S:{name};P:{password};;";
    using var qrCodeData = qrGenerator.CreateQrCode(payload, QRCodeGenerator.ECCLevel.Q);
    using var qrCode = new QRCode(qrCodeData);
    var qrCodeImage = qrCode.GetGraphic(20);
    qrCodeImage.MakeTransparent(qrCodeImage.GetPixel(0, 0));

    var result = new Bitmap(qrCodeImage.Width, qrCodeImage.Height, PixelFormat.Format32bppPArgb);
    result.MakeTransparent(result.GetPixel(0, 0));
    var graphics = Graphics.FromImage(result);
    graphics.Clear(Color.White);

    graphics.DrawImage(qrCodeImage, new Point(0, 0));

    var ellipsisSize = (int)Math.Round((result.Width + result.Height) / 2.0f * 0.25f);
    var rect = new Rectangle((int)Math.Round((result.Width - ellipsisSize) / 2.0f), (int)Math.Round((result.Height - ellipsisSize) / 2.0f), ellipsisSize, ellipsisSize);
    graphics.FillEllipse(new SolidBrush(Color.White), rect);
    var iconSize = (int)Math.Round((result.Width + result.Height) / 2.0f * 0.19f);
    rect = new Rectangle((int)Math.Round((result.Width - iconSize) / 2.0f), (int)Math.Round((result.Height - iconSize) / 2.0f), iconSize, iconSize);
    var iconRaw = new Bitmap("wifi.png");
    graphics.DrawImage(iconRaw, rect);

    return result;
}

void drawText(Graphics graphics, string text, PointF point, int fontSize)
{
    var font = new Font("Arial", fontSize);
    var textSize = graphics.MeasureString(text, font).ToSize();
    graphics.DrawString(text, font, new SolidBrush(Color.Black), new PointF(point.X - textSize.Width / 2.0f, point.Y - textSize.Height / 2.0f));
}
