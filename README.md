# WiFi QR-code generator

## Usage

```
=======================
Create QR-code for WiFi
=======================

Enter WiFi name: your-wifi-name
Enter WiFi password: YourPassw0rd
Enter WiFi authentication mode [0 = none, 1 = WEP, 2 = WPA, 3 = WPA2]: 3

Creating...
Saved as: your-wifi-name.png
```

<img src="your-wifi-name.png" alt="your-wifi-name.png" width="500" />

## Why there are different QR-codes for Android and iOS?

From the standard `WPA` and `WPA2` should have the same QR-code, but this does not work for some iPhones, it will not connect to a `WPA2` network if the QR-code specifies `WPA`.

References:

- [QRCoder - WiFi / PayloadGenerator.cs](https://github.com/codebude/QRCoder/wiki/Advanced-usage---Payload-generators#320-wifi)
- [Wikipedia - QR code - Joining a Wi‑Fi network](https://en.wikipedia.org/wiki/QR_code#Joining_a_Wi%E2%80%91Fi_network)
    - Common format: `WIFI:S:<SSID>;T:<WEP|WPA|blank>;P:<PASSWORD>;H:<true|false|blank>;`

## Use of icons

<div>Icons made by <a href="https://www.flaticon.com/authors/roundicons" title="Roundicons">Roundicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
